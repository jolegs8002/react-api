import React from "react";
import { Link } from "react-router-dom";
import { Card, Button, Col, Stack, Container, CardGroup, Row, Alert} from "react-bootstrap";

import { useCart } from "react-use-cart";
import { useState, useEffect} from "react"


const ProductCard = (props) => {


	const styles = {
	  card: {
	    backgroundColor: '#C0C0C0',
	    borderRadius: 55,
	    padding: '3rem'
	  },
	  cardImage: {
	    objectFit: 'cover',
	    borderRadius: 55
	  }
	}


	const { addItem } = useCart()

	// console.log(props.productProp._id)

	const [myProduct, setMyProduct] = useState()


	useEffect(() => {
	setMyProduct({
		id: props.id,
	  imageURL: props.image,
	  name: props.name,
	  description: props.description,
    stocks: props.stock,
	  price: props.price
	});
	}, []);

//console.log(props.image)

	return(
		
		<Container fluid>
      <CardGroup className="m-5 d-block">
        <Card className="m-5 border-0 shadow" style={styles.card}>
          <Row>
            <Col>
              <Card.Img src={`https://drive.google.com/uc?export=view&id=${props.productProp.imageURL}`} style={styles.cardImage} />
            </Col>
            <Col>
              <Card.Body>
		           	<Card.Title as="h6">{props.title}</Card.Title>
		            <Card.Text style={{fontSize:"12px"}}>
		               {props.description}
		            </Card.Text>  
		      </Card.Body>

		      <Card.Footer className="text-center p-3">
		        	<div className="container">
		                <Card.Text className="fw-bold text-start">
		    	               Price: &#8369;{props.price}<br/>
		    	               Stocks: {props.stocks}
		    	        </Card.Text>
		        	</div>

		        	<Button className="btn btn-danger m-2" onClick={() => addItem(myProduct)}>Add to Cart</Button>

		        	<Button as={Link} to={`/products/${props.id}`} className="margin-left btn btn-primary" >Buy Now</Button>
		        	
		        </Card.Footer>
            </Col>
          </Row>
        </Card>
      </CardGroup>
    </Container>


	)
}

export default ProductCard